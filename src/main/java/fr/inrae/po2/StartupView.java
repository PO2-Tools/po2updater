/*
 * Copyright (c) 2023. INRAE
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2;

import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.scene.shape.SVGPath;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.update4j.Configuration;
import org.update4j.FileMetadata;
import org.update4j.inject.InjectSource;
import org.update4j.inject.Injectable;
import org.update4j.service.UpdateHandler;

public class StartupView extends FXMLView implements UpdateHandler, Injectable {

    private Configuration config;

    @FXML
    private Label status;

    @FXML
    private Pane primary;

    @FXML
    private ProgressBar primaryProgress;

    @FXML
    private Pane secondary;

    @FXML
    private StackPane progressContainer;


    private boolean slow;

    @FXML
    private Button update;

    @FXML
    private Button launch;

    @FXML
    private ImageView background;

    private DoubleProperty primaryPercent;
    private DoubleProperty secondaryPercent;

    private BooleanProperty running;
    private volatile boolean abort;

    @InjectSource
    private Stage primaryStage;
    @InjectSource
    private Stage splashScreenStage;
    private Logger logger;
    String key = "";

    public StartupView(Configuration config, Stage splashScreenStage) {
        logger = LogManager.getLogger(PO2Updater.class);
        this.slow = false;
        this.update.setVisible(false);

        this.config = config;

        this.splashScreenStage = splashScreenStage;
        this.splashScreenStage.initStyle(StageStyle.UNDECORATED);

        background.setImage(new Image("file:resources_updater/Splash.png"));
        this.primaryStage = new Stage();


        primaryPercent = new SimpleDoubleProperty(this, "primaryPercent");
        secondaryPercent = new SimpleDoubleProperty(this, "secondaryPercent");

        running = new SimpleBooleanProperty(this, "running");
//        primary.setVisible(true);
//        primary.setStyle("-fx-background-color: #0021ff");
        secondary.setVisible(false);
//        secondary.setStyle("-fx-background-color: #ffb700");

        primaryProgress.progressProperty().bind(primaryPercent);

//        primary.maxWidthProperty().bind(progressContainer.widthProperty().multiply(primaryPercent));
//        secondary.maxWidthProperty().bind(progressContainer.widthProperty().multiply(secondaryPercent));

        status.setOpacity(0);
        FadeTransition fade = new FadeTransition(Duration.seconds(1.5), status);
        fade.setToValue(0);

        running.addListener((obs, ov, nv) -> {
            if (nv) {
                fade.stop();
                status.setOpacity(1);
            } else {
                fade.playFromStart();
                primaryPercent.set(0);
                secondaryPercent.set(0);
            }
        });

        primary.visibleProperty().bind(running);
        secondary.visibleProperty().bind(primary.visibleProperty());
        update.fire();

    }

    @FXML
    void launchPressed(ActionEvent event) {
        logger.debug("Starting app");
        // setting the master pwd
        System.setProperty("PO2MasterKey", key);
        Task<Boolean> checkUpdates = checkUpdates();

        checkUpdates.setOnSucceeded(evt -> {
            Thread run = new Thread(() -> {
                config.launch(this);
            });

            //FIXME: add opt-out checkbox
            if (checkUpdates.getValue()) {
                ButtonType launch = new ButtonType("Launch", ButtonData.OK_DONE);
                Alert alert = new Alert(AlertType.CONFIRMATION);
                alert.setHeaderText("Update required");
                alert.setContentText("Application is not up-to-date, launch anyway?");
                alert.getButtonTypes().setAll(ButtonType.CANCEL, launch);

                alert.showAndWait().filter(bt -> bt == launch).ifPresent(bt -> {
                    run.start();
                });
            } else {
                run.start();
            }
        });

        run(checkUpdates);
    }

    @FXML
    void updatePressed(ActionEvent event) {
        logger.debug("trying to update");
        if (running.get()) {
            abort = true;
            return;
        }

        running.set(true);

        status.setText("Checking for updates...");

        Task<Boolean> checkUpdates = checkUpdates();
        checkUpdates.setOnSucceeded(evt -> {
            if (!checkUpdates.getValue()) {
                logger.debug("No updates found");
                status.setText("No updates found");
                running.set(false);
                launch.fire();
            } else {
                logger.debug("An update of PO²Manager was found. Downloading");
                Task<Void> doUpdate = new Task<>() {

                    @Override
                    protected Void call() throws Exception {
                        config.update((UpdateHandler) StartupView.this);
                        return null;
                    }

                };
                doUpdate.setOnSucceeded(evt2 -> {
                    launch.fire();
                });

                run(doUpdate);

            }
        });

        run(checkUpdates);
    }

    private Task<Boolean> checkUpdates() {
        return new Task<>() {

            @Override
            protected Boolean call() throws Exception {
                return config.requiresUpdate();
            }

        };
    }

    private void run(Runnable runnable) {
        Thread runner = new Thread(runnable);
        runner.setDaemon(true);
        runner.start();
    }

    /*
     * UpdateHandler methods
     */
    @Override
    public void updateDownloadFileProgress(FileMetadata file, float frac) throws AbortException {
        Platform.runLater(() -> {
            status.setText("Downloading " + file.getPath().getFileName() + " (" + ((int) (100 * frac)) + "%)");
            secondaryPercent.set(frac);
        });

        if (abort) {
            throw new AbortException();
        }
    }

    @Override
    public void updateDownloadProgress(float frac) throws InterruptedException {
        Platform.runLater(() -> primaryPercent.set(frac));

        if(slow)
            Thread.sleep(100);
    }

    @Override
    public void failed(Throwable t) {
        logger.error("Update failed", t);

        Platform.runLater(() -> {
            if (t instanceof AbortException)
                status.setText("Update aborted");
            else
                status.setText("Failed: " + t.getClass().getSimpleName() + ": " + t.getMessage());
        });
    }

    @Override
    public void succeeded() {
        Platform.runLater(() -> status.setText("Download complete"));
    }

    @Override
    public void stop() {
        Platform.runLater(() -> running.set(false));
        abort = false;
    }

}
