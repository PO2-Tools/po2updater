/*
 * Copyright (c) 2023. INRAE
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *
 * SPDX-License-Identifier: MIT
 */

package fr.inrae.po2;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.tools.ant.taskdefs.Manifest;
import org.apache.tools.ant.taskdefs.ManifestException;
import org.update4j.Configuration;

import java.io.*;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.util.Optional;
import java.util.Properties;

class PO2Updater {
    public static void main(String [] args) throws IOException, ManifestException {
        System.setProperty("log4j.configurationFile", "resources_updater"+File.separator+"log4j2.xml");
        System.setProperty("javax.net.ssl.trustStore", "resources_updater"+File.separator+"customCacerts");
        StartApp.main(args);
    }
}

public class StartApp extends Application {
    private static Logger logger;
    public static void main(String[] args) throws IOException, ManifestException {

        logger = LogManager.getLogger(PO2Updater.class);
        logger.debug("Starting PO2Updater");
        launch(args);
    }

    @Override
    public void start(Stage splashScreenStage) throws IOException {


        try {
            String url = checkForNewVersion();
            if(!url.isEmpty()) {

                Alert updateQuestion2 = new Alert(Alert.AlertType.CONFIRMATION);
                updateQuestion2.getDialogPane().setPrefSize(500, 180);
                TextArea ta = new TextArea("A new version of PO²Manager is available at : \n" + url);
                ta.setEditable(false);
                ta.setScrollLeft(0.0);
                ta.setWrapText(true);
                updateQuestion2.setTitle("New version available");
                updateQuestion2.setHeaderText(null);
//                updateQuestion2.setContentText("A new version of PO²Manager is available at : \n" + url);
                updateQuestion2.getDialogPane().setContent(ta);
                updateQuestion2.setResizable(true);

                ButtonType download = new ButtonType("Download");
                updateQuestion2.getButtonTypes().setAll(ButtonType.NO, download);

                Optional<ButtonType> result = updateQuestion2.showAndWait();
                if(result.get() == download) {
                    this.getHostServices().showDocument(url);
                    Platform.exit();
                }
            } else {
                logger.debug("PO²Updater is up to date ");
            }
        } catch (Exception e) {
            logger.error("checking for update failed : " + e .getMessage());
        }

        Properties propFile = new Properties();
        Path updatePath = Paths.get(Utils.ResoucesPath+"update.properties");
        if(!Files.exists(updatePath)) {
            updatePath = Paths.get("resources_updater"+File.separator+"update.properties");
        }
        propFile.load(new FileInputStream(updatePath.toFile()));
        URL configUrl = new URL(propFile.getProperty("url"));
        Configuration config = null;
        logger.debug("Loading setup.xml from " + configUrl);
        try (Reader in = new InputStreamReader(configUrl.openStream(), StandardCharsets.UTF_8)) {
            config = Configuration.read(in);
            // on copie le fichier en local au cas où on démarre sans connexion une prochaine fois
            Files.createDirectories(Paths.get(Utils.ResoucesPath));
            Files.copy(configUrl.openStream(),Paths.get(Utils.ResoucesPath+"setup.xml"), StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            logger.error(e.getMessage());
            logger.error("Could not load remote config, falling back to local.");
            try (Reader in = Files.newBufferedReader(Paths.get(Utils.ResoucesPath+"setup.xml"))) {
                config = Configuration.read(in);
            }
        }
        StartupView startup = new StartupView(config, splashScreenStage);
        Scene scene = new Scene(startup);
        splashScreenStage.setScene(scene);
        splashScreenStage.show();
    }

    private String checkForNewVersion() throws IOException, ManifestException {

        HttpClientBuilder httpBuilder = null;
        Boolean updateAvailable = false;
        String updateVersion = "";
        String downloadURL = "";

        // checking for updater app update

        Manifest m = new Manifest(new FileReader("resources_updater"+File.separator+"manifest"));
        Manifest.Section section = m.getSection("app_info");
        Manifest.Attribute attrType = section.getAttribute("Compilation-format");
        Manifest.Attribute attrManifestUpdate = section.getAttribute("Updating-URL");
        Manifest.Attribute attrDStamp = section.getAttribute("Compilation-DSTAMP");
        Manifest.Attribute attrTStamp = section.getAttribute("Compilation-TSTAMP");
        Manifest.Attribute attrVersion = section.getAttribute("Specification-Version");
        Manifest.Attribute attrCompDate = section.getAttribute("Compilation-Date");
        Manifest.Attribute attrChangeLogURL = section.getAttribute("Changelog-URL");

        if (attrVersion != null && !"".equals(attrVersion.getValue())) {
            String versionDate = "Version : " + attrVersion.getValue() + " - " + attrCompDate.getValue();
            logger.debug("loading PO²Updater : " + versionDate);
        }

        try {
            SSLContextBuilder builder = new SSLContextBuilder();
            builder.loadTrustMaterial(null, new TrustSelfSignedStrategy());

            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(
                    builder.build(), NoopHostnameVerifier.INSTANCE);
            httpBuilder = HttpClientBuilder.create().setSSLSocketFactory(sslsf);


        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }

        HttpClient client = httpBuilder.build();
        HttpGet get = new HttpGet(attrManifestUpdate.getValue());
        HttpResponse response = client.execute(get);

        final HttpEntity entity = response.getEntity();
        if (entity != null) {
            try (InputStream inputStream = entity.getContent()) {
                BufferedInputStream bis = new BufferedInputStream(inputStream);

                Manifest onlineManifest = new Manifest(new InputStreamReader(bis));
                Manifest.Section onlineSection = onlineManifest.getSection("update-" + attrType.getValue());
                if(onlineSection != null) {
                    Manifest.Attribute onlineDStamp = onlineSection.getAttribute("Compilation-DSTAMP");
                    Manifest.Attribute onlineTStamp = onlineSection.getAttribute("Compilation-TSTAMP");
                    Manifest.Attribute onlineVersion = onlineSection.getAttribute("Specification-Version");
                    updateVersion = onlineVersion.getValue();
                    Integer myDStamp = Integer.parseInt(attrDStamp.getValue());
                    Integer myTStamp = Integer.parseInt(attrTStamp.getValue());
                    Integer onDStamp = Integer.parseInt(onlineDStamp.getValue());
                    Integer onTStamp = Integer.parseInt(onlineTStamp.getValue());


                    if (myDStamp < onDStamp) {
                        updateAvailable = true;
                    } else {
                        if (myDStamp.equals(onDStamp) && myTStamp < onTStamp) {
                            updateAvailable = true;
                        }
                    }
                    if (updateAvailable) {
                        downloadURL = onlineSection.getAttribute("Download-URL").getValue();
                    }
                }


            }
        }
        return downloadURL;
    }
}
