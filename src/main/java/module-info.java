module updater {
    requires org.update4j;
    requires java.sql;
    requires java.net.http;
    requires javafx.base;
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.graphics;
    requires org.apache.logging.log4j;
    requires ant;
    requires org.apache.httpcomponents.httpclient;
    requires org.apache.httpcomponents.httpcore;

    opens fr.inrae.po2;

    exports fr.inrae.po2;
}
