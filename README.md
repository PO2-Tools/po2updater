## About PO²Updater

This project allows you to launch the latest version of PO²Manger. Its main purpose is to download an online configuration file in order to download the latest version of PO²Manager with its dependencies and launch it.

The update process is done using update4J api